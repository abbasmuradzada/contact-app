const contactList = document.querySelector('.contact-list');
const addBtn = document.querySelector('.add-contact');
const editFormBtn = document.querySelector('.edit-contact');
const nameInput = document.getElementById("name-input")
const numberInput = document.getElementById("number-input")


let deleteBtns;
let editBtns

let numberList = [
    {
        id: 1,
        name: "Elmar Qasimov",
        number: 554131836,
        time: '17:40'
    },
    {
        id: 2,
        name: "Abbas Muradzada",
        number: 557580680,
        time: '17:40'

    },
    {
        id: 3,
        name: "Semyon Penziyev",
        number: 555555555,
        time: '17:40'
    }
]

if (localStorage.getItem("list") === null) {
    localStorage.setItem("list", JSON.stringify(numberList));
}
let localStorageList = JSON.parse(localStorage.getItem("list"));

const renderContactList = () => {
    contactList.innerHTML='';
    localStorageList.forEach((element, index) => {
        const personName = document.createElement('h5');
        personName.className='card-title';
        personName.innerText = element.name;

        const personNumber = document.createElement('p');
        personNumber.className='card-text';
        personNumber.innerText = element.number;

        const deleteBtn = document.createElement('a');
        deleteBtn.className='btn btn-danger delete-button';
        deleteBtn.innerText = "Delete";

        const editBtn = document.createElement('a');
        editBtn.className='btn btn-warning edit-button';
        editBtn.innerText = "Edit";

        const addTime = document.createElement('sup');
        // editBtn.className='btn btn-warning edit-button';
        addTime.innerText = element.time;

        const cardBody = document.createElement('div');
        cardBody.className='card-body';
        cardBody.append(personName, personNumber, editBtn, deleteBtn);

        const card = document.createElement('div');
        card.className='card';
        card.append(cardBody, addTime);

        editBtn.addEventListener('click', () => {
            editFormBtn.classList.remove('disabled');
            editContact(localStorageList[index]);
        })

        deleteBtn.addEventListener('click', () => {
            deleteContact(index);
        })

        contactList.appendChild(card);   
    });
}
renderContactList();


const editContact = (contact) => {
    nameInput.value = contact.name;
    numberInput.value = contact.number;
        editFormBtn.addEventListener('click', (e) => {
            e.preventDefault();
            contact.name = nameInput.value;
            contact.number = numberInput.value;
            // console.log(contact.name);
            // console.log(contact.number);
            nameInput.value = ''
            numberInput.value = ''
            localStorage.setItem("list", JSON.stringify(localStorageList));
            // localStorageList = JSON.parse(localStorage.getItem("list"))
            editFormBtn.classList.add('disabled');
            renderContactList();
        })
}


const deleteContact = (index) => {
    localStorageList.splice(index, 1);
    if (localStorageList.length == 0) {
        localStorageList = []
    }
    localStorage.setItem("list", JSON.stringify(localStorageList));
    renderContactList();
}

const addContact = () => {
    addBtn.addEventListener('click', (e) => {
        e.preventDefault();
        let date = new Date();
        const newContact = {
            id: localStorageList.length + 1,
            name: nameInput.value,
            number: numberInput.value,
            time: ` ${`${date.getHours()}`.length > 1 ? date.getHours(): `0${date.getHours()}`}:${`${date.getMinutes()}`.length > 1 ? date.getMinutes():`0${date.getMinutes()}`}`
        }
        if (nameInput.value.length > 0 && numberInput.value.length >= 9) {
            localStorageList.push(newContact)
            nameInput.value = ''
            numberInput.value = ''
            localStorage.setItem("list", JSON.stringify(localStorageList));
            localStorageList = JSON.parse(localStorage.getItem("list"))
            renderContactList();
        }else{
            console.log("incorrect validation");           
        }
    })
}
addContact();